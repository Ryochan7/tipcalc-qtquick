import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0


ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Tip Calc")

    function formatAmount(amount)
    {
        amount = amount.toFixed(2).replace(/./g, function(c, i, a) {
            return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
        });

        return amount;
    }

    function calculateSplitPerPerson()
    {
        var temp = amountInput.numAmount / 100.0 + (amountInput.numAmount / 100.0 * (Number(tipPercentageEdit.text) * 0.01));
        temp = temp / Number(splitNumEdit.text);
        return temp;
    }

    property real fontSizeMulti: {
        var temp = Screen.devicePixelRatio;
        if (Screen.devicePixelRatio > 1.0)
        {
            temp = temp * 0.75;
        }

        return temp;
    }

    header: ToolBar {
        /*Text {
            anchors.left: parent.left
            anchors.leftMargin: 10

            text: "Willard"
            font.pointSize: 14 * fontSizeMulti
            font.bold: true
            anchors.verticalCenter: parent.verticalCenter
        }
        */

        ToolButton {
            text: "..."
            font.pointSize: 14 * fontSizeMulti
            font.bold: true
            anchors.right: parent.right
            rotation: 90.0

            onClicked: {
                optionsMenu.open();
            }
        }

        Menu {
            id: optionsMenu
            transformOrigin: Menu.TopRight
            x: parent.width - width

            MenuItem {
                text: "Exit"
                onTriggered: Qt.quit();
            }
        }
    }

    Flickable {
        anchors.fill: parent

        anchors.topMargin: 20
        anchors.bottomMargin: 40
        //anchors.margins: 20
        //contentWidth: parent.width
        contentHeight: mainPage.childrenRect.height

        flickableDirection: Flickable.VerticalFlick

        //ScrollBar.vertical: ScrollBar {
            //anchors.right: parent.right
            //anchors.rightMargin: 40
            //opacity: 1.0
        //}

        ScrollIndicator.vertical: ScrollIndicator {
        }

        Item {
            id: mainPage
            //anchors.fill: parent
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: 20
            anchors.rightMargin: 20

            RowLayout {
                id: amountRow

                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right

                Label {
                    text: "Total Amount:"

                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti
                }

                TextField {
                    id: amountInput
                    readOnly: true
                    font.pointSize: 12 * fontSizeMulti
                    Layout.preferredWidth: 150

                    function inputDigit(digitString)
                    {
                        var temp = Number(digitString);
                        var oldNumStr = Number(numAmount).valueOf().toString();
                        console.log(oldNumStr);
                        console.log("LONE STAR");
                        if (oldNumStr.length < 9)
                        {
                            oldNumStr = oldNumStr + digitString;
                            numAmount = Number(oldNumStr);
                        }
                    }

                    function removeDigit()
                    {
                        var temp = Number(numAmount).toString();
                        temp = temp.slice(0, temp.length-1);
                        numAmount = Number(temp);
                    }

                    function resetField()
                    {
                        numAmount = 0;
                    }


                    property string numStr: "0.00"
                    //property real numAmount: 0.00
                    property int numAmount: 0
                    property real numAmountReal: numAmount / 100.0
                    text: "$ " + formatAmount(numAmountReal);
                    //horizontalAlignment: Text.AlignRight
                }
            }

            RowLayout {
                id: tipRow

                anchors.top: amountRow.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.topMargin: 20

                spacing: 20

                Label {
                    text: "Tip (%):"

                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti
                }

                Button {
                    text: "-"
                    Layout.preferredWidth: 60
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        var value = tipPercentageEdit.text.valueOf();
                        if (value > 0 && value <= 99)
                        {
                            value--;
                            tipPercentageEdit.text = value.toString();
                        }
                    }
                }

                TextField {
                    id: tipPercentageEdit
                    text: "15"
                    readOnly: true
                    Layout.preferredWidth: 40
                    font.pointSize: 12 * fontSizeMulti
                    horizontalAlignment: Text.AlignHCenter
                    maximumLength: 2
                    validator: IntValidator {

                    }
                }

                Button {
                    text: "+"
                    Layout.preferredWidth: 60
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        var value = tipPercentageEdit.text.valueOf();
                        if (value >= 0 && value < 99)
                        {
                            value++;
                            tipPercentageEdit.text = value.toString();
                        }
                    }
                }
            }

            RowLayout {
                id: splitBillRow

                anchors.top: tipRow.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.topMargin: 20

                spacing: 20

                Label {
                    text: "Split Bill:"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti
                }

                Button {
                    text: "-"
                    Layout.preferredWidth: 60
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        var value = splitNumEdit.text.valueOf();
                        if (value > 1 && value <= 99)
                        {
                            value--;
                            splitNumEdit.text = value.toString();
                        }
                    }
                }

                TextField {
                    id: splitNumEdit
                    text: "1"
                    readOnly: true
                    Layout.preferredWidth: 40
                    font.pointSize: 12 * fontSizeMulti
                    horizontalAlignment: Text.AlignHCenter
                    maximumLength: 2
                    validator: IntValidator {

                    }
                }

                Button {
                    text: "+"
                    Layout.preferredWidth: 60
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        var value = splitNumEdit.text.valueOf();
                        if (value >= 0 && value < 99)
                        {
                            value++;
                            splitNumEdit.text = value.toString();
                        }
                    }
                }
            }

            RowLayout {
                id: totalPayRow

                anchors.top: splitBillRow.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.topMargin: 20

                Label {
                    text: "Total to Pay:"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti
                    font.bold: true
                }

                Label {
                    id: totalToPayLabel
                    property string prefix: "$"
                    property string numStr: "0.00"
                    text: "$" + formatAmount(amountInput.numAmountReal + ((amountInput.numAmountReal) * (Number(tipPercentageEdit.text) * 0.01)))
                    font.pointSize: 12 * fontSizeMulti
                    font.bold: true
                }
            }

            RowLayout {
                id: totalTipRow

                anchors.top: totalPayRow.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.topMargin: 20

                Label {
                    text: "Total Tip:"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti
                    font.bold: true
                }

                Label {
                    id: totalTipLabel
                    property string prefix: "$"
                    property string numStr: "0.00"
                    text: "$" + formatAmount(amountInput.numAmountReal * (Number(tipPercentageEdit.text) * 0.01))
                    font.pointSize: 12 * fontSizeMulti
                    font.bold: true
                }
            }

            RowLayout {
                id: totalPerPersonRow

                anchors.top: totalTipRow.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.topMargin: 20

                Label {
                    text: "Total Per Person:"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti
                    font.bold: true
                }

                Label {
                    id: totalPerPersonLabel
                    property string numStr: "0.00"
                    text: "$" + formatAmount(calculateSplitPerPerson())
                    font.pointSize: 12 * fontSizeMulti
                    font.bold: true
                }
            }

            GridLayout {
                id: numPadLayout

                anchors.top: totalPerPersonRow.bottom
                anchors.left: parent.left
                anchors.right: parent.right

                anchors.topMargin: 30

                columns: 3
                rows: 4
                columnSpacing: 10
                rowSpacing: 10

                Button {
                    text: "1"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        amountInput.inputDigit(text);
                    }
                }

                Button {
                    text: "2"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        amountInput.inputDigit(text);
                    }
                }

                Button {
                    text: "3"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        amountInput.inputDigit(text);
                    }
                }

                Button {
                    text: "4"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        amountInput.inputDigit(text);
                    }
                }

                Button {
                    text: "5"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        amountInput.inputDigit(text);
                    }
                }

                Button {
                    text: "6"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        amountInput.inputDigit(text);
                    }
                }

                Button {
                    text: "7"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        amountInput.inputDigit(text);
                    }
                }

                Button {
                    text: "8"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        amountInput.inputDigit(text);
                    }
                }

                Button {
                    text: "9"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        amountInput.inputDigit(text);
                    }
                }

                Button {
                    text: "Reset"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        amountInput.resetField();
                    }
                }

                Button {
                    text: "0"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        amountInput.inputDigit(text);
                    }
                }

                Button {
                    text: "Del"
                    Layout.fillWidth: true
                    font.pointSize: 12 * fontSizeMulti

                    onClicked: {
                        amountInput.removeDigit();
                    }
                }
            }
        }
    }
}
