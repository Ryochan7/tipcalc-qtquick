# tipcalc-qtquick

A clone of a tip calculator app that I use on Android. This version is written
with Qt Quick.

This program is licensed under the [DBAD license](https://github.com/philsturgeon/dbad/blob/master/LICENSE.md).
